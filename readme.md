# Front End Eval

A sample project for evaluating various front-end stacks.

- ClojureScript (see [cljs-hello](./cljs-hello))
- TypeScript
- Elm
- Reason
- ES6

Instructions for each should be in their respective readmes.