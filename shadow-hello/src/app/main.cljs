(ns app.main
  (:require [app.lib :as lib]
            [re-frame.core :as rf]
            [reagent.core :as reagent]))

; Global state -------------------------
(def db {:name "Chris" :email "cd@example.com"})


; Events -------------------------------
(rf/reg-event-db
 :initialize-db #(identity db))

(defn field-change [db [_ k v]]
  (assoc db k v))

(rf/reg-event-db :field-change field-change)


; Subscriptions -----------------------
(rf/reg-sub :name #(:name %))
(rf/reg-sub :email #(:email %))


; Views -------------------------------
(defn main-panel []
  (let [name @(rf/subscribe [:name])
        email @(rf/subscribe [:email])]
    [:div.card
      [:header.card-header
        [:h1.card-title name]
        [:span.card-subtitle email]]
      [:form {:on-change #(rf/dispatch [:field-change (-> % .-target .-name keyword) (-> % .-target .-value)])}
        [:div.field-group
          [:label.field-label "Name"]
          [:input.field {:value name
                         :name "name"
                         :placeholder "Name"}]]
        [:div.field-group
          [:label.field-label "Email"]
          [:input.field {:value email
                         :name "email"
                         :placeholder "Email"}]]]]))

; Entry -----------------------------------

(defn mount-root []
  (rf/clear-subscription-cache!)
  (reagent/render [main-panel]
                  (.querySelector js/document "main")))

(defn ^:export init []
  (rf/dispatch-sync [:initialize-db])
  (mount-root))

(init)
