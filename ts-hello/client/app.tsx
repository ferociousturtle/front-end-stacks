import * as React from 'react';
import UserCard from './user-card';

export default function() {
  return (
    <main className='main-container'>
      <UserCard />
    </main>
  );
}
