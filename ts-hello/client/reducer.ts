import { userCard, UserCardState } from './user-card/reducer';
import { combineReducers } from 'redux';

export interface AppState {
  userCard: UserCardState;
}

export default combineReducers({
  userCard,
});
