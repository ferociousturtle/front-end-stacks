import { userCard, UserCardState } from '../reducer';
import { setField } from '../action';

const defaultState: UserCardState = {
  email: 'hoi@there.com',
  name: 'Hoi',
};

describe('The user card reducer', () => {
  it('defaults to dummy values', () => {
    const state = userCard.apply(null, [null, {}]) as UserCardState;
    expect(state.name).toEqual('Jim Bob');
    expect(state.email).toEqual('jbob@example.com');
  });

  it('returns current state if action is unknown', () => {
    const state = userCard.apply(null, [defaultState, {}]) as UserCardState;
    expect(state).toEqual(defaultState);
  });

  it('updates the name correctly', () => {
    const s1 = userCard(defaultState, setField('name', 'Greg'));
    expect(s1.name).toEqual('Greg');
    expect(s1.email).toEqual(defaultState.email);
    expect(defaultState.name).toEqual('Hoi'); // Immutability check
  });

  it('updates the email correctly', () => {
    const s1 = userCard(defaultState, setField('email', 'a@b.com'));
    expect(s1.email).toEqual('a@b.com');
    expect(s1.name).toEqual(defaultState.name);
    expect(defaultState.email).toEqual('hoi@there.com'); // Immutability check
  });
});
