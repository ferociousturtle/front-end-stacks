import * as React from 'react';
import { mount, render } from 'enzyme';
import { RawView as UserCard } from '../index';
import { setField } from '../action';

describe('The <UserCard /> Component', () => {
  it('renders the current name and email', () => {
    const wrapper = mount(<UserCard email='foo@bar.com'
                                    name='Fooby'
                                    setField={setField} />);
    const title = wrapper.findWhere((w) => w.hasClass('card-title'));
    const subTitle = wrapper.findWhere((w) => w.hasClass('card-sub-title'));
    expect(title.text().trim()).toEqual('Fooby');
    expect(subTitle.text().trim()).toEqual('foo@bar.com');
  });

  it('renders the inputs', () => {
    const wrapper = render(<UserCard email='fallon@nbc.com'
                                    name='Jimmy'
                                    setField={setField} />);
    const nameInput = wrapper.find('input[name=name]');
    const emailInput = wrapper.find('input[name=email]');

    expect(nameInput.attr('value')).toEqual('Jimmy');
    expect(emailInput.attr('value')).toEqual('fallon@nbc.com');
  });
});
