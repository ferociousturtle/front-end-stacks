enum Action {
  SetField,
}

export default Action;

export interface SetField {
  type: Action.SetField;
  name: string;
  value: string;
}

export function setField(name: string, value: string): SetField {
  return {
    name,
    type: Action.SetField,
    value,
  };
}
