import * as React from 'react';
import { connect } from 'react-redux';
import { setField, SetField } from './action';
import { UserCardState } from './reducer';
import { AppState } from '../reducer';

interface Props {
  name: string;
  email: string;
  setField: (name: string, email: string) => SetField;
}

function mapStateToProps(state: AppState): UserCardState {
  return state.userCard;
}

const mapDispatchToProps = {
  setField,
};

const View = connect(mapStateToProps, mapDispatchToProps)(RawView);

export default View;

export function RawView(props: Props) {
  const {name, email} = props;

  return (
    <div className='card'>
      <header className='card-header'>
        <h1 className='card-title'>
          {name}
        </h1>
        <span className='card-sub-title'>
          {email}
        </span>
      </header>
      <form className='card-body'
            onChange={(e) => {
              const target = e.target as HTMLInputElement;
              props.setField(target.name, target.value);
            }}>
        <div className='field-group'>
          <label className='field-label'>
            Name
          </label>
          <input className='field'
                 name='name'
                 value={name}
                 placeholder='Name' />
        </div>
        <div className='field-group'>
          <label className='field-label'>
            Email
          </label>
          <input className='field'
                 name='email'
                 value={email}
                 placeholder='Email' />
        </div>
      </form>
    </div>
  );
}
