import { default as Action, SetField } from './action';

export interface UserCardState {
  name: string;
  email: string;
}

export function userCard(state: UserCardState, action: SetField): UserCardState {
  switch (action.type) {
  case Action.SetField:
    return {
      ...state,
      [action.name]: action.value,
    };
  default:
    return state || {
      email: 'jbob@example.com',
      name: 'Jim Bob',
    };
  }
}
