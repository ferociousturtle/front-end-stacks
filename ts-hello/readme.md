## TypeScript

## Pros/Cons

PROS:

- Momentum
- Tooling
- Short put from existing skill-set
- No-undefindes/nulls
- Easier refactoring (due to static typing)
- Documentation
- Big community
- Debugging
- Test suite is fast and has excellent output
- Faster (but still slow) boot time (~7 secs vs ~24 secs)

CONS:

- Most of the weaknesses of JS
- Not immutable by default
- Verbose
- More complex environment (npm, webpack, etc)

## Running

- Run: `npm start`
- Test: `npm test -- --watchAll`

## Credit

Based loosely on this:

https://github.com/mattnedrich/react-redux-typescript