# cljs-hello

A [re-frame](https://github.com/Day8/re-frame) application designed to ... well, that part is up to you.

## Pros/Cons

PROS:

- Pragmatic balance of encouraging immutability while still allowing mutations
- Easy JS interop (much better than any statically typed language I've used)
- Data-oriented development is superior to anything I've seen
- The parenthesis are actually a strength
  - The langauge is very consistent and unambiguous
  - The language itself is data and can be manipulated as data (enabling very powerful abstractions)
  - With parinfer, the negative aspects of parenthesis become a total non-issue
- The language and its culture focuses on simplicity as defined here:
  - https://www.infoq.com/presentations/Simple-Made-Easy
- The standard library has a lot of handy functions, eliminating the need for lots of libraries (e.g. ramda, lodash, etc)
- Welcoming, and active community
- Highly testable code by default
- Repl-driven exploration of code is *much* better than Rails or even JS
- Figwheel is shweet (hot code reloading)
- Automatically (well, almost) code optimization, minification, and splitting
- Mature-- much more so than even ES6, TypeScript
- Career: Clojure developers are among the top-paid
- Career: functional programming is on the upswing, and having experience in at least 1 functional language is a big plus
- Recruiting Clojure developers is easy-- it's a small pool, but there are a lot of people who *want* remote Clojure jobs
- Fun (at least to me)
- Stable-- the syntax doesn't change pretty much ever, and 2+ year old libraries still work


CONS (compared to JS/TS)

- Small community
- Tooling is subpar
- Full-fledged Clojure is sluggish to start (though you shouldn't have to do this often), and you still need it if you're going to get all of the ClojureScript build-tooling benefits
- Career: not much demand for Clojure
- Not as well documented/vetted as main-stream JavaScript
- Can't just copy/paste JS from jsBin or elsewhere (though, maybe this is a pro in disguise!)
- Errors are not great, to say the least, but diagnosing them is fairly simple when you get used to using the repl
- Slow boot time (~24 secs on my laptop)


## Development Mode

### Run application:

```
lein clean
rlwrap lein figwheel dev
```

Figwheel will automatically push cljs changes to the browser.

Wait a bit, then browse to [http://localhost:3449](http://localhost:3449).

### Run tests:

```
lein clean
lein doo node test once
lein doo node test
```

The above command assumes that you have [phantomjs](https://www.npmjs.com/package/phantomjs) installed. However, please note that [doo](https://github.com/bensu/doo) can be configured to run cljs.test in many other JS environments (chrome, ie, safari, opera, slimer, node, rhino, or nashorn).

## Production Build


To compile clojurescript to javascript:

```
lein clean
lein cljsbuild once min
```

## Resources

re-frame vs React: https://purelyfunctional.tv/article/react-vs-re-frame/

re-frame workshop: https://github.com/ClojureTO/JS-Workshop

common errors and their meaning: https://github.com/yogthos/clojure-error-message-catalog

figwheel and testing:

- https://github.com/bhauman/crashverse
- https://github.com/bhauman/lein-figwheel/issues/162
