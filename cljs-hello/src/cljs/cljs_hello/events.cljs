(ns cljs-hello.events
  (:require [re-frame.core :as rf]
            [cljs-hello.db :as db]))

(rf/reg-event-db
 :initialize-db
 (fn  [_ _]
   db/default-db))

(defn field-change [db [_ k v]]
  "field-change (field-change [db [evt key val]]) associates
  the specified key with the specified value in the map db"
  (assoc db k v))

(rf/reg-event-db :field-change field-change)
