(ns cljs-hello.views
  (:require [re-frame.core :as rf]))

(defn- sync-field [e]
  (rf/dispatch-sync [:field-change (-> e .-target .-name keyword) (-> e .-target .-value)]))

(defn main-panel []
  (let [name @(rf/subscribe [:name])
        email @(rf/subscribe [:email])]
    [:div.card
      [:header.card-header
        [:h1.card-title (str "Name: " name)]
        [:span.card-subtitle email]]
      [:form
        [:div.field-group
          [:label.field-label "Name"]
          [:input.field {:value name
                         :name "name"
                         :on-change sync-field
                         :placeholder "Name"}]]
        [:div.field-group
          [:label.field-label "Email"]
          [:input.field {:value email
                         :name "email"
                         :on-change sync-field
                         :placeholder "Email"}]]]]))
