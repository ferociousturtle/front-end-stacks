(ns cljs-hello.events-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [cljs-hello.events :refer [field-change]]))

(def ^:private db
  {:name "Hoi" :email "hoi@there.com"})

(deftest events-test
  (testing "Associates email"
    (is (= {:name "Jill" :email "hoi@there.com"}
           (field-change db [:evt :name "Jill"])))
    (is (= {:name "Hoi" :email "a@b.com"}
           (field-change db [:evt :email "a@b.com"])))))
