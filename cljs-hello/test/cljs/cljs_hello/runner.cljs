(ns cljs-hello.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [pjstadig.humane-test-output]
              [cljs-hello.events-test]))

(doo-tests 'cljs-hello.events-test)
